const usd = (state = 0, action) => {
  switch (action.type) {
    case 'USD':
      return action.usd;
    default:
      return state;
  }
};

export default usd;
