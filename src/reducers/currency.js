const currency = (state = 0, action) => {
  switch (action.type) {
    case 'UPDATE_CURRENCY':
      return action.newCurrency;
    default:
      return state;
  }
};

export default currency;
