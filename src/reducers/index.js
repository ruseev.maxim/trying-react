import { combineReducers } from 'redux'


import currency from './currency';
import usd from './usd';

const currencyApp = combineReducers({
  currency,
  usd,
});

export default currencyApp;
