import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './reducers';

// Components
import App from './components/App/App';



// import './style.sass';

const store = createStore(reducer);

console.log(store);

function App2() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}


ReactDOM.render(<App2 />, document.getElementById('root'));

// import { createStore } from 'redux';


// function counter(state = 0, action) {
//   switch (action.type) {
//     case 'INCREMENT':
//       return state + 1;
//     case 'DECREMENT':
//       return state - 1;
//     default:
//       return state;
//   }
// }

// let store = createStore(counter);

// store.subscribe(() => console.log(store.getState()));

// store.dispatch({ type: 'INCREMENT' });
