import React from 'react'
import {connect} from 'react-redux';

import {newCurrency} from '../../actions';

import './currency-button.sass'

class CurrencyButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = () => props.dispatch(newCurrency(Math.random()))
  }

  render() {
    return(
      <button className="currency-button" onClick={this.handleClick}>update currency</button>
    );
  };
}


CurrencyButton = connect()(CurrencyButton);

export default CurrencyButton;
