import React from 'react';

import CalculatedCurrency from '../CalculatedCurrency/CalculatedCurrency';
import CurrencyButton from '../CurrencyButton/CurrencyButton';
import ChooseCurrency from '../ChooseCurrency/ChooseCurrency';

const App = () => (
  <div>
    <CurrencyButton />
    <br/>
    <br/>
    <ChooseCurrency />
    <br/>
    <br/>
    <CalculatedCurrency />
  </div>

);

export default App;
