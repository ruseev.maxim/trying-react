import React from 'react';
import {connect} from 'react-redux';

let CalculatedCurrency = ({currency}) => {
  return (
    <strong>{currency ? currency : 'no currency'}</strong>
  );
};

const mapStateToProps = (state) => {
  console.log(state);
  return {currency: state.currency * state.usd}
};

CalculatedCurrency = connect(mapStateToProps)(CalculatedCurrency);

export default CalculatedCurrency;
