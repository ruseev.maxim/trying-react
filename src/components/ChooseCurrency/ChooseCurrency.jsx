import React from 'react';
import {connect} from 'react-redux';
import {usd} from '../../actions';

class ChooseCurrency extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = () => props.dispatch(usd());
  }

  render() {
    return(
      <button className="currency-button" onClick={this.handleClick}>usd</button>
    )
  }
}

ChooseCurrency = connect()(ChooseCurrency);

export default ChooseCurrency;
