export const newCurrency = (newCurrency) => ({
  type: 'UPDATE_CURRENCY',
  newCurrency
});

export const usd = () => ({
  type: 'USD',
  usd: Math.random(),
});
